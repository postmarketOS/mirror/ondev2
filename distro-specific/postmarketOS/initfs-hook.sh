#!/bin/sh
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Initramfs hook for postmarketOS

# Run a shell for debugging, used by: test/run-test-pmbootstrap-qemu.sh -r
if grep -q ONDEV2_RUN_INITFS_SHELL /proc/cmdline; then
	sh
fi

if grep -q ONDEV2_TEST /proc/cmdline; then
	sh /opt/run-test.sh
else
	ondev2
fi

echo
echo "NOTE: ondev2 has quit - dropping to shell"
echo

sh

while true; do
	sleep 1
done
