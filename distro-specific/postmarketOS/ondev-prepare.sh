#!/bin/sh -ex
# Copyright 2023 Oliver Smith
# SPDX-License-Identifier: GPL-3.0-or-later
#
# Glue code for "pmbootstrap install --ondev" to generate ondev2.cfg from
# deviceinfo and environment variables passed by pmbootstrap.

. /etc/deviceinfo

: ${ONDEV_CHANNEL:="v22.06"}
: ${ONDEV_CIPHER:=""}
: ${ONDEV_PMBOOTSTRAP_VERSION:="0.0.0"}
: ${ONDEV_UI:="plasma-mobile"}

# Minimum required pmbootstrap version check
check_pmbootstrap_version() {
	min="1.20.0"

	if [ "$ONDEV_PMBOOTSTRAP_VERSION" = "0.0.0" ]; then
		set +x
		echo "ERROR: do not run this script manually."
		echo "It's only meant to be called during" \
			"'pmbootstrap install --ondev'"
		exit 1
	fi

	version_result="$(apk version -t "$ONDEV_PMBOOTSTRAP_VERSION" "$min")"
	if [ "$version_result" = "=" ] || [ "$version_result" = ">" ]; then
		# Version check passed
		return
	elif [ "$version_result" = "<" ]; then
		set +x
		echo "ERROR: ondev2 requires pmbootstrap $min or higher." \
			"You are using $ONDEV_PMBOOTSTRAP_VERSION."
	else
		set +x
		echo "ERROR: failed to verify pmbootstrap version"
	fi
	exit 1
}

# Translate the UI value from "pmbootstrap config ui", which is the suffix of
# the postmarketos-ui-* pkgnames, to a prettier name.
pretty_ui() {
	case "$1" in
		gnome) echo "GNOME" ;;
		i3wm) echo "i3" ;;
		kodi) echo "Kodi" ;;
		mate) echo "MATE" ;;
		phosh) echo "Phosh" ;;
		plasma-bigscreen) echo "Plasma Bigscreen" ;;
		plasma-desktop) echo "Plasma Desktop" ;;
		plasma-mobile*) echo "Plasma Mobile" ;;
		shelli) echo "Shelli" ;;
		sway) echo "Sway" ;;
		sxmo) echo "Sxmo" ;;
		sxmo-de-dwm) echo "Sxmo (DWM)" ;;
		sxmo-de-sway) echo "Sxmo (Sway)" ;;
		weston) echo "Weston" ;;
		xfce4) echo "XFCE4" ;;
		*) echo "$1" ;;
	esac
    }

write_ondev2_cfg() {
	cipher_arg=""
	if [ -n "$ONDEV_CIPHER" ]; then
		cipher_arg="--cipher '$ONDEV_CIPHER'"
	fi

	cat <<- EOF > /etc/ondev2.cfg
	[ondev2]
	os_name = postmarketOS
	os_version = $ONDEV_CHANNEL

	user_interface = $(pretty_ui "$ONDEV_UI")

	cmd_luksformat = cryptsetup luksFormat --use-random $cipher_arg
	cmd_mkfs_root_ext4 = mkfs.ext4 -O '^metadata_csum,^huge_file' -L 'pmOS_root'

	# Busybox mount doesn't always detect the filesystem correctly
	cmd_mount_installdata = mount -o ro -t ext4

	cmd_post_install = ondev-post-install

	# Options below are generated from deviceinfo
	# https://postmarketos.org/deviceinfo
	EOF

	# Add deviceinfo_* options as device_* in ini format
	opts_from_deviceinfo="
		deviceinfo_arch
		deviceinfo_boot_filesystem
		deviceinfo_boot_part_start
		deviceinfo_cgpt_kpart
		deviceinfo_name
		deviceinfo_partition_type
		deviceinfo_storage_
	"
	for i in $opts_from_deviceinfo; do
		grep "^$i" /etc/deviceinfo \
			| sed -e "s/^deviceinfo_/device_/g" \
				-e 's/="/ = /g' \
				-e 's/"$//g' \
			>> /etc/ondev2.cfg
	done

	# Default for partition_type is msdos in deviceinfo, gpt in ondev2
	if ! grep -q "^device_partition_type =" /etc/ondev2.cfg; then
		echo "device_partition_type = msdos" >> /etc/ondev2.cfg
	fi
}

add_ondev2_cfg_overrides_from_test_cfg() {
	local test_cfg="/etc/ondev2.test.cfg"
	local cfg="/etc/ondev2.cfg"

	if ! [ -e "$test_cfg" ]; then
		return
	fi

	( echo
	  echo "#"
	  echo "# Overrides from $test_cfg"
	  echo "#"
	  echo
	  cat "$test_cfg" ) >> "$cfg"
}

add_test_deps() {
	if ! [ -e "/etc/ondev2.test.cfg" ]; then
		return
	fi

	apk add expect
}

add_debug_user() {
	if [ -e "/no-debug-user" ]; then
		set +x
		echo "NOTE: not creating debug user, because /no-debug-user exists"
		set -x
	else
		set +x
		echo "Creating debug user"
		echo "  username: 'user'"
		echo "  password: 'y'"
		echo
		echo "  This debug user will not be created if /no-debug-user"
		echo "  exists in the installation OS. Like this:"
		echo "  'pmbootstrap install --ondev --cp anyfile:/no-debug-user'"
		echo
		set -x
		yes | adduser user -G wheel || true
	fi
}

check_pmbootstrap_version
write_ondev2_cfg
add_ondev2_cfg_overrides_from_test_cfg
add_test_deps
add_debug_user

# HACK: /vmlinuz is assumed in grub.cfg instead of vmlinuz-virt,
# needs to be fixed in postmarketos-mkinitfs/boot-deploy
if [ -e /boot/vmlinuz-virt ] && ! [ -e /boot/vmlinuz ] ; then
	cp /boot/vmlinuz-virt /boot/vmlinuz
fi

# Generate new initfs with the updated ondev2.cfg
mkinitfs
