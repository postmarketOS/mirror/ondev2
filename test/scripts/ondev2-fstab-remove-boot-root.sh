#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
TEMP_DIR="$SCRIPT_DIR/../../_temp"
mkdir -p "$TEMP_DIR"

cp "$SCRIPT_DIR"/ondev2-fstab-remove-boot-root.in "$TEMP_DIR/fstab"

export OD2=1

"$SCRIPT_DIR"/../../src/scripts/ondev2-fstab-remove-boot-root.sh \
	"$TEMP_DIR"/fstab

if ! diff "$TEMP_DIR"/fstab "$SCRIPT_DIR"/ondev2-fstab-remove-boot-root.out; then
	echo
	echo "ERROR: test failed!"
	echo
	exit 1
fi

rm "$TEMP_DIR"/fstab
