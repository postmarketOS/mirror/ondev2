// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith, Johannes Marbach
#include "pages.h"
#include "ui.h"

#include "unl0kr/backends.h"
#include "unl0kr/command_line.h"
#include "unl0kr/config.h"
#include "unl0kr/indev.h"
#include "unl0kr/log.h"
#include "unl0kr/unl0kr.h"
#include "unl0kr/theme.h"
#include "unl0kr/themes.h"
#include "unl0kr/lv_drv_conf.h"

#if USE_FBDEV
#include "lv_drivers/display/fbdev.h"
#endif /* USE_FBDEV */
#if USE_DRM
#include "lv_drivers/display/drm.h"
#endif /* USE_DRM */

#include "lvgl/lvgl.h"
#include "lvgl/src/core/lv_disp.h"

#include "squeek2lvgl/sq2lv.h"

#include <libintl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/reboot.h>
#include <sys/time.h>
#include <unistd.h>

struct button_bottom {
	lv_obj_t *btn;
	lv_obj_t *btn_label;
};

static struct {
	ul_cli_opts cli_opts;
	ul_config_opts conf_opts;

	lv_disp_drv_t disp_drv;
	lv_disp_draw_buf_t disp_buf;

	uint32_t hor_res;
	uint32_t ver_res;

	lv_obj_t *keyboard;
	int keyboard_height;
	int padding;
	int label_width;

	bool is_alternate_theme;
	bool is_password_obscured;
	bool is_keyboard_toggled;

	lv_obj_t *container;

	lv_obj_t *back_btn;
	lv_obj_t *title_container;
	lv_obj_t *title_label;
	lv_obj_t *toggle_theme_btn;
	lv_obj_t *toggle_kb_btn;
	lv_obj_t *layout_dropdown;
	lv_obj_t *shutdown_btn;

	lv_obj_t *descr_container;
	lv_obj_t *descr_spangroup;
	lv_span_t *descr_span;

	lv_obj_t *pass_container;
	lv_obj_t *pass_textarea;

	lv_obj_t *dropdown_container;
	lv_obj_t *dropdown;

	lv_obj_t *progress;
	lv_obj_t *progress_container;
	lv_obj_t *progress_label;
	char progress_label_buf[5];

	struct button_bottom buttons_bottom[_OD2_BUTTON_MAX];
} g_gui;

static void set_theme(bool is_alternate)
{
	ul_theme_apply(ul_themes_themes[is_alternate
			 ? g_gui.conf_opts.theme.alternate_id
			 : g_gui.conf_opts.theme.default_id]);
}

static void toggle_theme(void)
{
	g_gui.is_alternate_theme = !g_gui.is_alternate_theme;
	set_theme(g_gui.is_alternate_theme);
}

static void toggle_theme_btn_clicked_cb(lv_event_t *event)
{
	LV_UNUSED(event);
	toggle_theme();
}

static void set_password_obscured(bool is_obscured)
{
	lv_obj_t *textarea = lv_keyboard_get_textarea(g_gui.keyboard);

	lv_textarea_set_password_mode(textarea, is_obscured);
	g_gui.is_password_obscured = is_obscured;
}

static void toggle_password_obscured(void)
{
	set_password_obscured(!g_gui.is_password_obscured);
}

static void toggle_pw_btn_clicked_cb(lv_event_t *event)
{
	LV_UNUSED(event);
	toggle_password_obscured();
}

static void set_keyboard_visibility(void)
{
	struct od2_page *page = od2_ui_page_current();
	bool page_has_keyboard = (page->input == OD2_INPUT_TEXT);
	bool show_keyboard = false;

	if (page_has_keyboard) {
		lv_obj_clear_flag(g_gui.toggle_kb_btn, LV_OBJ_FLAG_HIDDEN);

		show_keyboard = !ul_indev_is_keyboard_connected();
		if (g_gui.is_keyboard_toggled)
			show_keyboard = !show_keyboard;
	} else
		lv_obj_add_flag(g_gui.toggle_kb_btn, LV_OBJ_FLAG_HIDDEN);

	if (show_keyboard) {
		lv_obj_clear_flag(g_gui.layout_dropdown, LV_OBJ_FLAG_HIDDEN);
		lv_obj_clear_flag(g_gui.keyboard, LV_OBJ_FLAG_HIDDEN);
	} else {
		lv_obj_add_flag(g_gui.layout_dropdown, LV_OBJ_FLAG_HIDDEN);
		lv_obj_add_flag(g_gui.keyboard, LV_OBJ_FLAG_HIDDEN);
	}
}

static void toggle_keyboard_hidden(void)
{
	g_gui.is_keyboard_toggled = !g_gui.is_keyboard_toggled;
	set_keyboard_visibility();
}

static void toggle_kb_btn_clicked_cb(lv_event_t *event)
{
	LV_UNUSED(event);
	toggle_keyboard_hidden();
}

static void update_page_input_text_from_textarea(void)
{
	struct od2_page *page = od2_ui_page_current();

	if (page->input == OD2_INPUT_TEXT) {
		free(page->input_text);
		page->input_text = strdup(lv_textarea_get_text(g_gui.pass_textarea));
	}
}


static void page_btn_cb(lv_event_t *e)
{
	enum od2_button b;

	lv_obj_t *target = lv_event_get_target(e);

	if (target == g_gui.back_btn)
		return od2_ui_on_button(OD2_BUTTON_BACK);

	for (b = 0; b < _OD2_BUTTON_MAX; b++) {
		if (target == g_gui.buttons_bottom[b].btn) {
			if (b == OD2_BUTTON_CONTINUE)
				update_page_input_text_from_textarea();
			return od2_ui_on_button(b);
		}
	}
}

static void page_dropdown_cb(lv_event_t *e)
{
	LV_UNUSED(e);

	struct od2_dropdown *dropdown;
	uint16_t pos_sel = lv_dropdown_get_selected(g_gui.dropdown);
	uint16_t pos = 0;

	for (dropdown = od2_ui_page_current()->dropdown; dropdown->id; dropdown++) {
		if (dropdown->hidden)
			continue;

		if (pos == pos_sel)
			return od2_ui_on_dropdown(dropdown);

		pos++;
	}
}

static void layout_dropdown_value_changed_cb(lv_event_t *event)
{
	lv_obj_t *dropdown = lv_event_get_target(event);
	uint16_t idx = lv_dropdown_get_selected(dropdown);

	sq2lv_switch_layout(g_gui.keyboard, idx);
}

static void shutdown_mbox_value_changed_cb(lv_event_t *event)
{
	lv_obj_t *mbox = lv_event_get_current_target(event);

	if (lv_msgbox_get_active_btn(mbox) == 0) {
		sync();
		reboot(RB_POWER_OFF);
	}
	lv_msgbox_close(mbox);
}

static void shutdown_btn_clicked_cb(lv_event_t *event)
{
	LV_UNUSED(event);
	static const char *btns[] = { "Yes", "No", "" };
	lv_obj_t *mbox = lv_msgbox_create(NULL, NULL, "Shutdown device?", btns, false);

	lv_obj_set_width(mbox, 400);
	lv_obj_add_event_cb(mbox, shutdown_mbox_value_changed_cb, LV_EVENT_VALUE_CHANGED, NULL);
	lv_obj_center(mbox);
}

static void keyboard_value_changed_cb(lv_event_t *event)
{
	lv_obj_t *kb = lv_event_get_target(event);
	uint16_t btn_id = lv_btnmatrix_get_selected_btn(kb);

	if (btn_id == LV_BTNMATRIX_BTN_NONE)
		return;

	if (sq2lv_is_layer_switcher(kb, btn_id)) {
		sq2lv_switch_layer(kb, btn_id);
		return;
	}

	lv_keyboard_def_event_cb(event);
}

static void enter_pressed_cb(lv_event_t *event)
{
	LV_UNUSED(event);

	update_page_input_text_from_textarea();
	od2_ui_on_button_default();
}

static void create_header(void)
{
	lv_obj_t *header;
	lv_obj_t *back_btn_label;
	lv_obj_t *toggle_theme_btn_label;
	lv_obj_t *toggle_kb_btn_label;
	lv_obj_t *shutdown_btn_label;

	/* Header flexbox */
	header = lv_obj_create(g_gui.container);
	lv_obj_add_flag(header, UL_WIDGET_HEADER);
	lv_theme_apply(header); /* Force re-apply theme after setting flag so that the widget can be identified */
	lv_obj_set_flex_flow(header, LV_FLEX_FLOW_ROW);
	lv_obj_set_flex_align(header, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
	lv_obj_set_size(header, LV_PCT(100), LV_SIZE_CONTENT);

	/* Back button */
	g_gui.back_btn = lv_btn_create(header);
	lv_obj_add_event_cb(g_gui.back_btn, page_btn_cb, LV_EVENT_CLICKED,
			    NULL);
	lv_obj_set_width(g_gui.back_btn, 50);
	back_btn_label = lv_label_create(g_gui.back_btn);
	lv_label_set_text(back_btn_label, UL_SYMBOL_CHEVRON_LEFT);
	lv_obj_center(back_btn_label);

	/* Page title */
	g_gui.title_container = lv_obj_create(header);
	lv_obj_set_size(g_gui.title_container, g_gui.label_width, 60);
	lv_obj_set_flex_grow(g_gui.title_container, 1);

	g_gui.title_label = lv_label_create(g_gui.title_container);
	lv_obj_set_align(g_gui.title_label, LV_ALIGN_LEFT_MID);
	lv_obj_set_style_pad_left(g_gui.title_label, 5, LV_PART_MAIN);

	/* Keyboard layout dropdown */
	g_gui.layout_dropdown = lv_dropdown_create(header);
	lv_dropdown_set_options(g_gui.layout_dropdown, sq2lv_layout_short_names);
	lv_obj_add_event_cb(g_gui.layout_dropdown,
			    layout_dropdown_value_changed_cb,
			    LV_EVENT_VALUE_CHANGED, NULL);
	lv_obj_set_width(g_gui.layout_dropdown, 90);

	/* Show / hide keyboard button */
	g_gui.toggle_kb_btn = lv_btn_create(header);
	lv_obj_add_event_cb(g_gui.toggle_kb_btn, toggle_kb_btn_clicked_cb,
			    LV_EVENT_CLICKED, NULL);
	toggle_kb_btn_label = lv_label_create(g_gui.toggle_kb_btn);
	lv_label_set_text(toggle_kb_btn_label, LV_SYMBOL_KEYBOARD);
	lv_obj_center(toggle_kb_btn_label);

	/* Theme switcher button */
	g_gui.toggle_theme_btn = lv_btn_create(header);
	lv_obj_add_event_cb(g_gui.toggle_theme_btn,
			    toggle_theme_btn_clicked_cb, LV_EVENT_CLICKED,
			    NULL);
	toggle_theme_btn_label = lv_label_create(g_gui.toggle_theme_btn);
	lv_label_set_text(toggle_theme_btn_label, UL_SYMBOL_ADJUST);
	lv_obj_center(toggle_theme_btn_label);


	/* Shutdown button */
	g_gui.shutdown_btn = lv_btn_create(header);
	lv_obj_add_event_cb(g_gui.shutdown_btn, shutdown_btn_clicked_cb,
			    LV_EVENT_CLICKED, NULL);
	shutdown_btn_label = lv_label_create(g_gui.shutdown_btn);
	lv_label_set_text(shutdown_btn_label, LV_SYMBOL_POWER);
	lv_obj_center(shutdown_btn_label);
}

static void create_descr(void)
{
	/* Description container */
	g_gui.descr_container = lv_obj_create(g_gui.container);
	lv_obj_set_size(g_gui.descr_container, g_gui.label_width, LV_PCT(100));
	lv_obj_set_flex_grow(g_gui.descr_container, 1);

	/* Description label */
	g_gui.descr_spangroup = lv_spangroup_create(g_gui.descr_container);
	lv_spangroup_set_align(g_gui.descr_spangroup, LV_TEXT_ALIGN_LEFT);
	lv_spangroup_set_mode(g_gui.descr_spangroup, LV_SPAN_MODE_BREAK);
	lv_spangroup_set_overflow(g_gui.descr_spangroup, LV_SPAN_OVERFLOW_ELLIPSIS);

	g_gui.descr_span = lv_spangroup_new_span(g_gui.descr_spangroup);
	lv_obj_set_style_max_height(g_gui.descr_spangroup, LV_PCT(100), LV_PART_MAIN);
	lv_obj_set_align(g_gui.descr_spangroup, LV_ALIGN_TOP_MID);
}

static void create_input_password(void)
{
	lv_obj_t *toggle_pw_btn;
	lv_obj_t *toggle_pw_btn_label;
	int textarea_height;

	g_gui.is_password_obscured = true;

	/* Textarea flexbox */
	g_gui.pass_container = lv_obj_create(g_gui.container);
	lv_obj_set_size(g_gui.pass_container, LV_PCT(100), LV_SIZE_CONTENT);
	lv_obj_set_style_max_width(g_gui.pass_container, 512, LV_PART_MAIN);
	lv_obj_set_flex_flow(g_gui.pass_container, LV_FLEX_FLOW_ROW);
	lv_obj_set_flex_align(g_gui.pass_container, LV_FLEX_ALIGN_CENTER,
			      LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
	lv_obj_set_style_pad_left(g_gui.pass_container, g_gui.padding, LV_PART_MAIN);
	lv_obj_set_style_pad_right(g_gui.pass_container, g_gui.padding, LV_PART_MAIN);

	/* Textarea */
	g_gui.pass_textarea = lv_textarea_create(g_gui.pass_container);
	lv_textarea_set_one_line(g_gui.pass_textarea, true);
	lv_textarea_set_password_mode(g_gui.pass_textarea, true);
	lv_textarea_set_password_bullet(g_gui.pass_textarea, g_gui.conf_opts.textarea.bullet);
	lv_textarea_set_placeholder_text(g_gui.pass_textarea, "Enter password...");
	lv_obj_add_event_cb(g_gui.pass_textarea, enter_pressed_cb, LV_EVENT_READY, NULL);
	lv_obj_set_flex_grow(g_gui.pass_textarea, 1);
	lv_obj_add_state(g_gui.pass_textarea, LV_STATE_FOCUSED);

	/* Route physical keyboard input into textarea */
	lv_group_t *keyboard_input_group = lv_group_create();
	ul_indev_set_keyboard_input_group(keyboard_input_group);
	lv_group_add_obj(keyboard_input_group, g_gui.pass_textarea);

	/* Reveal / obscure password button */
	toggle_pw_btn = lv_btn_create(g_gui.pass_container);
	textarea_height = lv_obj_get_height(g_gui.pass_textarea);
	lv_obj_set_size(toggle_pw_btn, textarea_height, textarea_height);
	toggle_pw_btn_label = lv_label_create(toggle_pw_btn);
	lv_obj_center(toggle_pw_btn_label);
	lv_label_set_text(toggle_pw_btn_label, LV_SYMBOL_EYE_OPEN);
	lv_obj_add_event_cb(toggle_pw_btn, toggle_pw_btn_clicked_cb, LV_EVENT_CLICKED, NULL);
}

static void create_input_dropdown(void)
{
	g_gui.dropdown_container = lv_obj_create(g_gui.container);
	lv_obj_set_size(g_gui.dropdown_container, LV_PCT(100), LV_SIZE_CONTENT);
	lv_obj_set_style_max_width(g_gui.dropdown_container, 512, LV_PART_MAIN);

	g_gui.dropdown = lv_dropdown_create(g_gui.dropdown_container);
	lv_obj_add_event_cb(g_gui.dropdown, page_dropdown_cb,
			    LV_EVENT_VALUE_CHANGED, NULL);
	lv_obj_set_size(g_gui.dropdown, LV_PCT(100), LV_SIZE_CONTENT);
}

static void create_input_progress(void)
{
	g_gui.progress_container = lv_obj_create(g_gui.container);
	lv_obj_set_size(g_gui.progress_container, LV_PCT(100), LV_SIZE_CONTENT);
	lv_obj_set_style_max_width(g_gui.progress_container, 512, LV_PART_MAIN);

	g_gui.progress = lv_bar_create(g_gui.progress_container);
	lv_obj_set_size(g_gui.progress, LV_PCT(100), LV_SIZE_CONTENT);

	g_gui.progress_label = lv_label_create(g_gui.progress);
	lv_obj_center(g_gui.progress_label);
}

static void create_buttons_bottom(void)
{
	lv_obj_t *btn;
	lv_obj_t *btn_label;
	enum od2_button b;

	for (b = 0; b < _OD2_BUTTON_MAX; b++) {
		btn = lv_btn_create(g_gui.container);
		lv_obj_set_size(btn, LV_PCT(100), 80);
		lv_obj_set_style_max_width(btn, 512, LV_PART_MAIN);

		lv_obj_add_event_cb(btn, page_btn_cb, LV_EVENT_CLICKED, NULL);

		btn_label = lv_label_create(btn);
		lv_obj_center(btn_label);

		g_gui.buttons_bottom[b].btn = btn;
		g_gui.buttons_bottom[b].btn_label = btn_label;
	}
}

void od2_ui_gui_init(void)
{
	uint32_t dpi = 0;
	size_t buf_size;
	lv_color_t *buf;
	int lang_dropdown_height;

	/* Parse command line options */
	// ul_cli_parse_opts(argc, argv, &g_gui.cli_opts);

	/* Set up log level */
	if (g_gui.cli_opts.verbose)
		ul_log_set_level(UL_LOG_LEVEL_VERBOSE);

	/* Announce ourselves */
	ul_log(UL_LOG_LEVEL_VERBOSE, "unl0kr %s", UL_VERSION);

	/* Parse config files */
	ul_config_init_opts(&g_gui.conf_opts);
	ul_config_parse_files(g_gui.cli_opts.config_files,
			g_gui.cli_opts.num_config_files, &g_gui.conf_opts);

	/* Initialise LVGL and set up logging callback */
	lv_init();
	lv_log_register_print_cb(ul_log_print_cb);

	/* Initialise display driver */
	lv_disp_drv_init(&g_gui.disp_drv);

	/* Initialise framebuffer driver and query display size */
	switch (g_gui.conf_opts.general.backend) {
#if USE_FBDEV
	case UL_BACKENDS_BACKEND_FBDEV:
		fbdev_init();
		fbdev_force_refresh(true);
		fbdev_get_sizes(&g_gui.hor_res, &g_gui.ver_res, &dpi);
		g_gui.disp_drv.flush_cb = fbdev_flush;
		break;
#endif /* USE_FBDEV */
#if USE_DRM
	case UL_BACKENDS_BACKEND_DRM:
		drm_init();
		drm_get_sizes((lv_coord_t *)&g_gui.hor_res, (lv_coord_t *)&g_gui.ver_res, &dpi);
		g_gui.disp_drv.flush_cb = drm_flush;
		break;
#endif /* USE_DRM */
	default:
		ul_log(UL_LOG_LEVEL_ERROR, "Unable to find suitable backend");
		exit(EXIT_FAILURE);
	}

	/* Override display parameters with command line options if necessary */
	if (g_gui.cli_opts.hor_res > 0)
		g_gui.hor_res = LV_MIN(g_gui.hor_res,
				       (uint32_t)g_gui.cli_opts.hor_res);
	if (g_gui.cli_opts.ver_res > 0)
		g_gui.ver_res = LV_MIN(g_gui.ver_res,
				       (uint32_t)g_gui.cli_opts.ver_res);
	if (g_gui.cli_opts.dpi > 0)
		dpi = g_gui.cli_opts.dpi;

	/* Prepare display buffer */
	/* At least 1/10 of the display size is recommended */
	buf_size = g_gui.hor_res * g_gui.ver_res;
	buf = (lv_color_t *)malloc(buf_size * sizeof(lv_color_t));
	lv_disp_draw_buf_init(&g_gui.disp_buf, buf, NULL, buf_size);

	/* Register display driver */
	g_gui.disp_drv.draw_buf = &g_gui.disp_buf;
	g_gui.disp_drv.hor_res = g_gui.hor_res;
	g_gui.disp_drv.ver_res = g_gui.ver_res;
	g_gui.disp_drv.dpi = dpi;
	lv_disp_drv_register(&g_gui.disp_drv);

	/* Connect input devices */
	ul_indev_auto_connect();

	/* Initialise theme */
	set_theme(g_gui.is_alternate_theme);

	/* Prevent scrolling when keyboard is off-screen */
	lv_obj_clear_flag(lv_scr_act(), LV_OBJ_FLAG_SCROLLABLE);

	/* Figure out a few numbers for sizing and positioning */
	g_gui.keyboard_height = g_gui.ver_res > g_gui.hor_res ? g_gui.ver_res / 3 : g_gui.ver_res / 2;
	g_gui.padding = g_gui.keyboard_height / 8;
	g_gui.label_width = g_gui.hor_res - 2 * g_gui.padding;

	/* Main flexbox */
	g_gui.container = lv_obj_create(lv_scr_act());
	lv_obj_set_flex_flow(g_gui.container, LV_FLEX_FLOW_COLUMN);
	lv_obj_set_flex_align(g_gui.container, LV_FLEX_ALIGN_CENTER,
			      LV_FLEX_ALIGN_CENTER, LV_FLEX_ALIGN_CENTER);
	lv_obj_set_size(g_gui.container, LV_PCT(100), g_gui.ver_res);
	lv_obj_set_pos(g_gui.container, 0, 0);
	lv_obj_set_style_pad_row(g_gui.container, g_gui.padding, LV_PART_MAIN);
	lv_obj_set_style_pad_bottom(g_gui.container, g_gui.padding, LV_PART_MAIN);

	create_header();
	create_descr();
	create_input_password();
	create_input_dropdown();
	create_input_progress();

	/* Set header button size to match dropdown (for some reason the height
	 * is only available here) */
	lang_dropdown_height = lv_obj_get_height(g_gui.layout_dropdown);
	lv_obj_set_size(g_gui.toggle_theme_btn, lang_dropdown_height, lang_dropdown_height);
	lv_obj_set_size(g_gui.toggle_kb_btn, lang_dropdown_height, lang_dropdown_height);
	lv_obj_set_size(g_gui.shutdown_btn, lang_dropdown_height, lang_dropdown_height);

	create_buttons_bottom();

	/* Keyboard (after textarea / label so that key popovers are not drawn over) */
	g_gui.keyboard = lv_keyboard_create(lv_scr_act());
	lv_keyboard_set_mode(g_gui.keyboard, LV_KEYBOARD_MODE_TEXT_LOWER);
	lv_keyboard_set_textarea(g_gui.keyboard, g_gui.pass_textarea);
	lv_obj_remove_event_cb(g_gui.keyboard, lv_keyboard_def_event_cb);
	lv_obj_add_event_cb(g_gui.keyboard, keyboard_value_changed_cb, LV_EVENT_VALUE_CHANGED, NULL);
	lv_obj_add_event_cb(g_gui.keyboard, enter_pressed_cb, LV_EVENT_READY, NULL);
	lv_obj_set_size(g_gui.keyboard, g_gui.hor_res, g_gui.keyboard_height);
	ul_theme_prepare_keyboard(g_gui.keyboard);

	/* Apply textarea options */
	set_password_obscured(g_gui.conf_opts.textarea.obscured);

	/* Apply keyboard options */
	sq2lv_switch_layout(g_gui.keyboard, g_gui.conf_opts.keyboard.layout_id);
	lv_dropdown_set_selected(g_gui.layout_dropdown, g_gui.conf_opts.keyboard.layout_id);
	if (g_gui.conf_opts.keyboard.popovers)
		lv_keyboard_set_popovers(g_gui.keyboard, true);
}


uint32_t ul_get_tick(void)
{
	static uint64_t start_ms;
	struct timeval tv_now, tv_start;
	uint64_t now_ms;
	uint32_t time_ms;

	if (start_ms == 0) {
		gettimeofday(&tv_start, NULL);
		start_ms = (tv_start.tv_sec * 1000000 + tv_start.tv_usec) / 1000;
	}

	gettimeofday(&tv_now, NULL);
	now_ms = (tv_now.tv_sec * 1000000 + tv_now.tv_usec) / 1000;

	time_ms = now_ms - start_ms;
	return time_ms;
}

static void fill_input_dropdown(struct od2_page *page)
{
	struct od2_dropdown *dropdown;
	uint32_t pos = 0;

	lv_dropdown_clear_options(g_gui.dropdown);

	for (dropdown = page->dropdown; dropdown->id; dropdown++) {
		if (dropdown->hidden)
			continue;

		lv_dropdown_add_option(g_gui.dropdown, gettext(dropdown->name), pos);

		if (dropdown == page->dropdown_selected)
			lv_dropdown_set_selected(g_gui.dropdown, pos);

		pos++;
	}
}

void od2_ui_gui_show_page(struct od2_page *page)
{
	lv_obj_t *btn_label;
	uint8_t i = 0;
	uint8_t id;

	/* update title and descr */
	lv_label_set_text(g_gui.title_label, gettext(page->title));
	lv_span_set_text(g_gui.descr_span, od2_page_get_descr(page));
	lv_obj_set_size(g_gui.descr_spangroup, g_gui.label_width,
			lv_spangroup_get_expand_height(g_gui.descr_spangroup,
			g_gui.label_width));

	/* hide all inputs */
	lv_obj_add_flag(g_gui.pass_container, LV_OBJ_FLAG_HIDDEN);
	lv_obj_add_flag(g_gui.dropdown_container, LV_OBJ_FLAG_HIDDEN);
	lv_obj_add_flag(g_gui.progress_container, LV_OBJ_FLAG_HIDDEN);

	/* hide all buttons */
	lv_obj_set_style_opa(g_gui.back_btn, LV_OPA_TRANSP, LV_PART_MAIN);
	for (i = 0; i < _OD2_BUTTON_MAX; i++)
		lv_obj_add_flag(g_gui.buttons_bottom[i].btn,
				LV_OBJ_FLAG_HIDDEN);

	/* show input */
	switch (page->input) {
	case OD2_INPUT_NONE_OR_DROPDOWN:
		if (page->dropdown) {
			fill_input_dropdown(page);
			lv_obj_clear_flag(g_gui.dropdown_container,
					  LV_OBJ_FLAG_HIDDEN);
		}
		break;
	case OD2_INPUT_TEXT:
		if (page->is_password) {
			set_password_obscured(true);
			lv_textarea_set_text(g_gui.pass_textarea,
					     page->input_text);
			lv_obj_clear_flag(g_gui.pass_container,
					  LV_OBJ_FLAG_HIDDEN);
		}
		break;
	case OD2_INPUT_PROGRESS:
		lv_obj_clear_flag(g_gui.progress_container, LV_OBJ_FLAG_HIDDEN);

		snprintf(g_gui.progress_label_buf,
			 sizeof(g_gui.progress_label_buf), "%i%%",
			 page->progress);
		lv_label_set_text(g_gui.progress_label, g_gui.progress_label_buf);
		lv_bar_set_value(g_gui.progress, page->progress, LV_ANIM_ON);
		break;
	}

	/* show buttons */
	for (i = 0; page->buttons[i]; i++) {
		id = page->buttons[i];

		if (id == OD2_BUTTON_BACK) {
			lv_obj_set_style_opa(g_gui.back_btn, LV_OPA_COVER, LV_PART_MAIN);
		} else {
			btn_label = g_gui.buttons_bottom[id].btn_label;
			/* update text in case locale changed */
			lv_label_set_text(btn_label, od2_page_get_button_text(id));
			lv_obj_clear_flag(g_gui.buttons_bottom[id].btn,
					  LV_OBJ_FLAG_HIDDEN);
		}
	}

	set_keyboard_visibility();
	lv_disp_trig_activity(NULL);
}
