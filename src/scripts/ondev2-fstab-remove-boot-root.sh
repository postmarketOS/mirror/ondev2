#!/bin/sh -e
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023 Oliver Smith
# Remove root "/" and boot "/boot" entries from an /etc/fstab file, so ondev2
# can write its own entries at the end. This is an extra script so 'make check'
# can test it.

if [ -z "$OD2" ]; then
	echo "ERROR: this script should only be called from inside ondev2"
	exit 1
fi

if ! [ -f "$1" ]; then
	echo "usage: $(basename $0) /path/to/etc/fstab"
	exit 1
fi

# Root partition /
sed -i '/^[^# \t]*[ \t]*\/[ \t]/d' "$1"

# Boot partition /boot
sed -i '/^[^# \t]*[ \t]*\/boot[ \t]/d' "$1"
