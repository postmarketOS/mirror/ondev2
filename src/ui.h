/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include "pages.h"

#define INTERACT_LOOP_SLEEP_MS 5000

void od2_ui_init(enum od2_page_id initial);
void od2_ui_show_page(void);
void od2_ui_interact(void);
void od2_ui_interact_once(void);
void od2_ui_on_button(enum od2_button id);
void od2_ui_on_button_default(void);
void od2_ui_on_dropdown(struct od2_dropdown *dropdown);
struct od2_page *od2_ui_page_current(void);

void od2_ui_navigate_to_finished(void);
void od2_ui_navigate_to_failed(void);

/* Get the user's choices */
bool od2_ui_is_fde_selected(void);
enum od2_storage_id od2_ui_get_target_storage(void);
const char *od2_ui_get_fde_pass(void);
