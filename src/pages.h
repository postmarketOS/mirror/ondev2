/* SPDX-License-Identifier: GPL-3.0-or-later
 * Copyright 2023 Oliver Smith */
#pragma once
#include "storages.h"
#include <stdbool.h>
#include <stdint.h>

#define OD2_MAX_BUTTONS_PER_PAGE 10

enum od2_input {
	OD2_INPUT_NONE_OR_DROPDOWN,
	OD2_INPUT_TEXT,
	OD2_INPUT_PROGRESS,
};

enum od2_page_id {
	OD2_PAGE_LANGUAGE,
	OD2_PAGE_TRANSLATION_INCOMPLETE,
	OD2_PAGE_SIMPLE_OR_ADVANCED,
	OD2_PAGE_STORAGE,
	OD2_PAGE_OVERWRITE_WARNING,
	OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED,
	OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED_CONFIRM,
	OD2_PAGE_ADVANCED_FILESYSTEM,
	OD2_PAGE_ADVANCED_USER_PASS,
	OD2_PAGE_ADVANCED_USER_PASS_CONFIRM,
	OD2_PAGE_ADVANCED_FDE,
	OD2_PAGE_ADVANCED_FDE_USER_PASS_OR_NOT,
	OD2_PAGE_ADVANCED_FDE_PASS,
	OD2_PAGE_ADVANCED_FDE_PASS_CONFIRM,
	OD2_PAGE_FINAL_CONFIRM,
	OD2_PAGE_WAIT,
	OD2_PAGE_FINISHED,
	OD2_PAGE_FINISHED_REMOVE_SD,
	OD2_PAGE_ERROR_INSTALL_FAILED,
	OD2_PAGE_ERROR_PASS_DOESNT_MATCH,
	_OD2_PAGE_MAX,
};

enum od2_button {
	OD2_BUTTON_NONE, /* mark the end of button arrays */

	/* Keep in sync with enum od2_storage_id */
	OD2_BUTTON_STORAGE_EMMC = OD2_STORAGE_EMMC,
	OD2_BUTTON_STORAGE_NVME = OD2_STORAGE_NVME,
	OD2_BUTTON_STORAGE_SD = OD2_STORAGE_SD,

	OD2_BUTTON_BACK,
	OD2_BUTTON_CONTINUE,
	OD2_BUTTON_TRY_AGAIN,

	OD2_BUTTON_SIMPLE,
	OD2_BUTTON_ADVANCED,

	OD2_BUTTON_ENABLE,
	OD2_BUTTON_DISABLE,

	OD2_BUTTON_FDE_USER_PASS,
	OD2_BUTTON_FDE_SEPARATE_PASS,

	_OD2_BUTTON_MAX,
};

struct od2_dropdown {
	const char *id; /* for text UI, must not be a number */
	const char *name;
	const char *value;
	bool hidden;
};

struct od2_page {
	enum od2_page_id id;
	enum od2_input input;

	const char *title;
	const char *descr;

	struct od2_dropdown *dropdown;
	struct od2_dropdown *dropdown_selected;

	bool is_password;
	/* Text written by user into the text or password input, is never NULL
	 * for input == OD2_INPUT_TEXT after od2_ui_init() ran. */
	char *input_text;
	/* Progress in percent */
	uint8_t progress;

	enum od2_button buttons[OD2_MAX_BUTTONS_PER_PAGE + 1];
	enum od2_button button_selected;
};

extern struct od2_page g_pages[];
extern const char *g_button_text[];

extern struct od2_dropdown g_dropdown_locale[];
extern struct od2_dropdown g_dropdown_filesystem[];

const char *od2_page_get_descr(struct od2_page *page);
const char *od2_page_get_button_text(enum od2_button id);

void od2_page_storage_update_buttons(void);
