// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "config.h"
#include "misc.h"
#include "pages.h"
#include <ini.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CONFIG_DEFAULTS_SYSTEM "/usr/share/ondev2/defaults.cfg"
#define CONFIG_DEFAULTS_CWD "defaults.cfg"

struct od2_config g_config = {};

#define stringify(x) #x
#define load_key_or_ret(key) \
do { \
	if (strcmp(name, stringify(key)) == 0) { \
		if (g_config.key) \
			free(g_config.key); \
		g_config.key = strdup(value); \
		return 1; \
	} \
} while (0)

static int load_entry(void *ud, const char *section, const char *name,
		      const char *value)
{
	UNUSED(ud);

	if (strcmp(section, "ondev2") != 0)
		/* unknown section */
		return 0;

	load_key_or_ret(device_arch);
	load_key_or_ret(device_name);

	load_key_or_ret(device_storage_emmc);
	load_key_or_ret(device_storage_emmc_repartition);
	load_key_or_ret(device_storage_nvme);
	load_key_or_ret(device_storage_nvme_repartition);
	load_key_or_ret(device_storage_sd);

	load_key_or_ret(device_partition_type);
	load_key_or_ret(device_boot_part_start);
	load_key_or_ret(device_boot_part_size);
	load_key_or_ret(device_boot_filesystem);

	load_key_or_ret(device_cgpt_kpart);
	load_key_or_ret(device_cgpt_kpart_start);
	load_key_or_ret(device_cgpt_kpart_size);

	load_key_or_ret(os_name);
	load_key_or_ret(os_version);

	load_key_or_ret(user_interface);

	load_key_or_ret(update_fstab_crypttab);

	load_key_or_ret(cmd_luksformat);
	load_key_or_ret(cmd_mkfs_root_ext4);
	load_key_or_ret(cmd_mount_installdata);

	load_key_or_ret(cmd_post_install);
	load_key_or_ret(cmd_reboot);
	load_key_or_ret(cmd_poweroff);

	load_key_or_ret(mountpoint);

	/* unknown name */
	return 0;
}

#undef load_key_or_ret

static int load_file(const char *path)
{
	if (ini_parse(path, load_entry, NULL) == 0)
		return 0;

	printf("ERROR: failed to parse config file: %s\n", path);
	return -1;
}

int od2_config_load(const char *path)
{
	const char *path_default = "/usr/share/ondev2/defaults.cfg";

	/* Load system's defaults.cfg if it exists. Otherwise, assume
	 * development environment and load defaults.cfg from the current
	 * working dir. */
	if (access(path_default, F_OK) != 0)
		path_default = "defaults.cfg";
	if (load_file(path_default) < 0)
		return -1;

	/* Overwrite the defaults with the user's config entries where set */
	if (load_file(path) < 0)
		return -1;

	return 0;
}
