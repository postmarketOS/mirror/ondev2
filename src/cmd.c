// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

int run_cmd(const char *format, ...)
{
	int rc;
	char cmd[1000];
	va_list argptr;

	va_start(argptr, format);
	rc = vsnprintf(cmd, sizeof(cmd), format, argptr);
	va_end(argptr);

	if (rc >= (int) sizeof(cmd)) {
		printf("ERROR: %s: invalid format string or resulting cmd too"
		       " long (vsnprintf rc=%i): %s\n", __func__, rc,
		       format);
		return -1;
	}

	printf("+ %s\n", cmd);
	rc = system(cmd);

	if (rc == -1) {
		perror("ERROR: system() failed");
		return -1;
	}

	if (WIFEXITED(rc) != true) {
		printf("ERROR: command failed\n");
		return -1;
	}

	if (WEXITSTATUS(rc) != 0) {
		printf("ERROR: command failed with exit code: %i\n", WEXITSTATUS(rc));
		return -1;
	}

	return 0;
}

