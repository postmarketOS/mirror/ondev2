// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "config.h"
#include "pages.h"
#include <assert.h>
#include <ctype.h>
#include <libintl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Statically initialized strings that shall be translated */
#define t(str) str

struct od2_dropdown g_dropdown_locale[] = {
	{ .id = "en", .name = "English", .value = "en_US.UTF-8" },
	{ .id = "de", .name = "German", .value = "de_DE.UTF-8" },
	{},
};

struct od2_dropdown g_dropdown_filesystem[] = {
	{ .id = "e", .name = "EXT4", .value = "ext4" },
	{ .id = "b", .name = "BTRFS", .value = "btrfs" },
	{ .id = "f", .name = "F2FS", .value = "f2fs" },
	{},
};

struct od2_page g_pages[] = {
	{
		.id = OD2_PAGE_LANGUAGE,
		.title = t("Welcome"),
		.descr = t("You are about to install %s %s (%s) on your %s."
			   " Select a language."),
		.dropdown = g_dropdown_locale,
		.buttons = { OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		/* This page only makes sense when it's translated ;) */
		.id = OD2_PAGE_TRANSLATION_INCOMPLETE,
		.title = t("Translation Incomplete"),
		.descr = t("The translations into the selected language are"
			   " incomplete. Consider switching back to English."),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_SIMPLE_OR_ADVANCED,
		.title = t("Advanced Options"),
		.descr = t("Do you want to configure advanced options "
			   " (hostname, filesystem, ...)?"),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_SIMPLE,
			     OD2_BUTTON_ADVANCED,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_STORAGE,
		.title = t("Select Storage"),
		.descr = t("Where do you want to install %s?"),
		/* buttons: see od2_page_storage_update_buttons() */
	},
	{
		.id = OD2_PAGE_OVERWRITE_WARNING,
		.title = t("Overwriting Internal Storage"),
		.descr = t("WARNING: you are about to overwrite all data on"
			   " the internal storage. Are you sure?"),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED,
		.title = t("Password"),
		.descr = t("Choose a password. It will be used to encrypt your"
			   " device, and for unlocking your device. Make sure"
			   " to remember this password or write it down,"
			   " without it the data on your device cannot be"
			   " accessed. If you want two separate passwords, go"
			   " back and select advanced options."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_SIMPLE_USER_FDE_PASS_COMBINED_CONFIRM,
		.title = t("Confirm Password"),
		.descr = t("Please type the same password again to confirm."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_FILESYSTEM,
		.title = t("Filesystem"),
		.descr = t("Which filesystem do you want to use?"),
		.dropdown = g_dropdown_filesystem,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_USER_PASS,
		.title = t("User Password"),
		.descr = t("Choose the user password."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_USER_PASS_CONFIRM,
		.title = t("Confirm Password"),
		.descr = t("Please type the same password again to confirm."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_FDE,
		.title = t("Full Disk Encryption"),
		.descr = t("Do you want to enable full disk encryption?"),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_ENABLE,
			     OD2_BUTTON_DISABLE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_FDE_USER_PASS_OR_NOT,
		.title = t("FDE Password"),
		.descr = t("Use the previously typed in user password for full"
			   " disk encryption too, or use a separate"
			   " password?"),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_FDE_USER_PASS,
			     OD2_BUTTON_FDE_SEPARATE_PASS,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_FDE_PASS,
		.title = t("FDE Password"),
		.descr = t("Choose a full disk encryption password."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ADVANCED_FDE_PASS_CONFIRM,
		.title = t("Confirm Password"),
		.descr = t("Please type the same password again to confirm."),
		.input = OD2_INPUT_TEXT,
		.is_password = true,
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_FINAL_CONFIRM,
		.title = t("Ready To Go"),
		.descr = t("You are about to start the installation. If you"
			   " want to go back and change anything, now is the"
			   " chance."),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_WAIT,
		.title = t("Installing..."),
		.descr = t("Installation is in progress."),
		.input = OD2_INPUT_PROGRESS,
		.buttons = { OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_FINISHED,
		.title = t("Installation Successful"),
		.descr = t("You have successfully installed %s! Once you"
			   " continue, the device will reboot."),
		.buttons = { OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_FINISHED_REMOVE_SD,
		.title = t("Remove SD"),
		.descr = t("You have successfully installed %s! Once you"
			   " continue, the device will shutdown. Make sure to"
			   " remove the SD card before starting the device"
			   " again!"),
		.buttons = { OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ERROR_INSTALL_FAILED,
		.title = t("Installation Failed"),
		.descr = t("The installation has failed. Find troubleshooting"
			   " information at: %s"),
		.buttons = { OD2_BUTTON_CONTINUE,
			     OD2_BUTTON_NONE },
	},
	{
		.id = OD2_PAGE_ERROR_PASS_DOESNT_MATCH,
		.title = t("Password doesn't match"),
		.descr = t("The passwords do not match. Please try again."),
		.buttons = { OD2_BUTTON_BACK,
			     OD2_BUTTON_TRY_AGAIN,
			     OD2_BUTTON_NONE },
	},
};

const char *g_button_text[] = {
	[OD2_BUTTON_BACK] = t("Go Back"),
	[OD2_BUTTON_CONTINUE] = t("Continue"),
	[OD2_BUTTON_TRY_AGAIN] = t("Try Again"),
	[OD2_BUTTON_SIMPLE] = t("Simple Installation"),
	[OD2_BUTTON_ADVANCED] = t("Advanced Installation"),
	[OD2_BUTTON_ENABLE] = t("Enable"),
	[OD2_BUTTON_DISABLE] = t("Disable"),
	[OD2_BUTTON_FDE_USER_PASS] = t("Use same password"),
	[OD2_BUTTON_FDE_SEPARATE_PASS] = t("Use different password"),
	/* OD2_BUTTON_STORAGE: text is in g_storages[id].name */
};

const char *od2_page_get_button_text(enum od2_button id);

static char g_descr_buf[500];

const char *od2_page_get_descr(struct od2_page *page)
{
	const char *translated = gettext(page->descr);

	switch (page->id) {
	case OD2_PAGE_LANGUAGE:
		snprintf(g_descr_buf, sizeof(g_descr_buf),
			 translated,
			 g_config.os_name,
			 g_config.os_version,
			 g_config.user_interface,
			 g_config.device_name);
		break;
	case OD2_PAGE_STORAGE:
	case OD2_PAGE_FINISHED:
	case OD2_PAGE_FINISHED_REMOVE_SD:
		snprintf(g_descr_buf, sizeof(g_descr_buf),
			 translated,
			 g_config.os_name);
		break;
	default:
		if (strlen(translated) + 1 <= sizeof(g_descr_buf))
			strcpy(g_descr_buf, translated);
		else
			strcpy(g_descr_buf,
			       "(error: translation string too long)");
		break;
	}
	return g_descr_buf;
}

const char *od2_page_get_button_text(enum od2_button id)
{
	/* Values from enum od2_storage_id are in sync with enum od2_button */
	if (id > (enum od2_button)_OD2_STORAGE_UNKNOWN && id < (enum od2_button)_OD2_STORAGE_MAX)
		return g_storages[id].name;

	return gettext(g_button_text[id]);
}

void od2_page_storage_update_buttons(void)
{
	enum od2_storage_id id;
	enum od2_button *button = g_pages[OD2_PAGE_STORAGE].buttons;

	*button = OD2_BUTTON_BACK;
	button++;

	for (id = _OD2_STORAGE_UNKNOWN + 1; id < _OD2_STORAGE_MAX; id++) {
		if (!g_storages[id].available)
			continue;

		/* Values from enum od2_storage_id are in sync with enum
		 * od2_button */
		*button = (enum od2_button)id;
		button++;
	}

	*button = OD2_BUTTON_NONE;
}
