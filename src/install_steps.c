// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Oliver Smith
#include "cmd.h"
#include "config.h"
#include "storages.h"
#include "ui.h"
#include <dirent.h>
#include <libcryptsetup.h>
#include <mntent.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/statvfs.h>
#include <sys/types.h>
#include <unistd.h>

#define MOUNTPOINT_INSTALL_DATA "/tmp/ondev2/installdata"
#define MOUNTPOINT_ROOT "/tmp/ondev2/root"
#define CRYPTSETUP_ROOT_DEVICE_NAME "ondev2_root"
#define CRYPTSETUP_ROOT_DEVICE_PATH "/dev/mapper/ondev2_root"
#define MOUNTPOINT_BOOT "/tmp/ondev2/root/boot"
#define PATH_ETC_FSTAB MOUNTPOINT_ROOT "/etc/fstab"
#define PATH_ETC_CRYPTTAB MOUNTPOINT_ROOT "/etc/crypttab"

static const char *get_root_partition(bool with_luks)
{
	if (with_luks && od2_ui_is_fde_selected())
		return CRYPTSETUP_ROOT_DEVICE_PATH;
	return od2_storage_get_root_partition(od2_ui_get_target_storage());
}

static const char *get_boot_partition(void)
{
	return od2_storage_get_boot_partition(od2_ui_get_target_storage());
}

static const char *get_boot_mountpoint_installer(void)
{
	const char *ret = getenv("OD2_BOOT_MOUNTPOINT_INSTALLER");
	return ret ? : "/boot";
}

int od2_install_step_mount_install_data(void)
{
	const char *install_part = od2_storage_get_install_data_partition();

	run_or_ret("mkdir -p %s", MOUNTPOINT_INSTALL_DATA);
	run_or_ret("%s %s %s", g_config.cmd_mount_installdata, install_part,
		   MOUNTPOINT_INSTALL_DATA);

	return 0;
}

int od2_install_step_create_partition_table_cgpt(void)
{
	/* TODO: add logic from pmbootstrap's partition.py:partition_cgpt() */
	printf("ERROR: cgpt partitioning is not implemented yet\n");
	return -1;
}

int od2_install_step_create_partition_table_regular(void)
{
	enum od2_storage_id target_id = od2_ui_get_target_storage();
	const char *install_dev = od2_storage_get_path(target_id);
	const char *root_fs = "ext4"; /* FIXME: support multiple filesystems */

	run_or_ret("parted -s %s mktable %s", install_dev,
		   g_config.device_partition_type);

	run_or_ret("parted -s %s mkpart primary %s %ss %s", install_dev,
		   g_config.device_boot_filesystem,
		   g_config.device_boot_part_start,
		   g_config.device_boot_part_size);

	run_or_ret("parted -s %s mkpart primary %s %s 100%%", install_dev,
		   root_fs,
		   g_config.device_boot_part_size,
		   g_config.device_boot_part_size);

	run_or_ret("parted -s %s set 1 boot on", install_dev);

	return 0;
}

int od2_install_step_format_boot_partition_fs(void)
{
	const char *fs = g_config.device_boot_filesystem;
	const char *mkfs_cmd;

	if (strcmp(fs, "ext4") == 0)
		mkfs_cmd = "mkfs.ext4 -F";
	else if (strcmp(fs, "fat16") == 0)
		mkfs_cmd = "mkfs.fat -F 16";
	else if (strcmp(fs, "fat32") == 0)
		mkfs_cmd = "mkfs.fat -F 32";
	else {
		printf("ERROR: %s: can't map %s to mkfs cmd\n", __func__, fs);
		return -1;
	}

	run_or_ret("%s %s", mkfs_cmd, get_boot_partition());
	return 0;
}

int od2_install_step_format_root_partition_luks(void)
{
	const char *root_part = get_root_partition(false);
	const char *pass = od2_ui_get_fde_pass();
	struct crypt_device *cd;
	const char *cipher = "aes";
	const char *block_mode_iv = "xts-plain64";
	int key_size = 512;
	int rc;

	printf("Running crypt_format on %s\n", root_part);
	printf("  cipher: %s-%s\n", cipher, block_mode_iv);
	printf("  key size (in bytes): %i\n", key_size);

	rc = crypt_init(&cd, root_part);
	if (rc < 0) {
		printf("ERROR: crypt_init() failed: %i\n", rc);
		return rc; /* intentionally no crypt_free() */
	}

	rc = crypt_format(cd, CRYPT_LUKS2, cipher, block_mode_iv, NULL, NULL,
			  key_size / 8, NULL);
	if (rc < 0) {
		printf("ERROR: crypt_format() failed: %i\n", rc);
		goto done;
	}

	rc = crypt_keyslot_add_by_volume_key(cd, CRYPT_ANY_SLOT, NULL, 0,
					     pass, strlen(pass));
	if (rc < 0) {
		printf("ERROR: adding keyslot failed: %i\n", rc);
		goto done;
	}

done:
	crypt_free(cd);
	return rc;
}


int od2_install_step_mount_root_partition_luks(void)
{
	const char *root_part = get_root_partition(false);
	const char *pass = od2_ui_get_fde_pass();
	struct crypt_device *cd;
	int rc;

	printf("Mounting to cryptdevice %s\n", CRYPTSETUP_ROOT_DEVICE_NAME);

	rc = crypt_init(&cd, root_part);
	if (rc < 0) {
		printf("ERROR: crypt_init() failed: %i\n", rc);
		return rc; /* intentionally no crypt_free() */
	}

	rc = crypt_load(cd, CRYPT_LUKS, NULL);
	if (rc < 0) {
		printf("ERROR: crypt_load() failed: %i\n", rc);
		goto done;
	}

	rc = crypt_activate_by_passphrase(cd, CRYPTSETUP_ROOT_DEVICE_NAME,
					  CRYPT_ANY_SLOT, pass, strlen(pass),
					  0);
	if (rc < 0) {
		printf("ERROR: crypt_activate_by_passphrase() failed: %i\n", rc);
		goto done;
	}

done:
	crypt_free(cd);
	return rc;
}

int od2_install_step_format_root_partition_fs(void)
{
	const char *root_part = get_root_partition(true);

	/* FIXME: support multiple filesystems. Add od2_ui_get_filesystem() and
	 * a function in config.c that translates the selected FS string to the
	 * mkfs command. */

	run_or_ret("mkfs.ext4 -F %s", root_part);

	return 0;
}

int od2_install_step_mount_root(void)
{
	const char *root_part = get_root_partition(true);

	run_or_ret("mkdir -p %s", MOUNTPOINT_ROOT);
	run_or_ret("mount %s %s", root_part, MOUNTPOINT_ROOT);

	return 0;
}

int od2_install_step_copy_files_root(void)
{
	struct dirent *dp;
	DIR *dfd;

	dfd = opendir(MOUNTPOINT_INSTALL_DATA);
	if (!dfd) {
		perror("ERROR: opendir() failed");
		return -1;
	}

	while ((dp = readdir(dfd))) {
		/* Boot dir will be copied to the boot partition later. This
		 * is the point-of-no-return for same-storage installation as
		 * it replaces the boot partition with the on-device installer
		 * with the boot partition of the installed OS. */
		if (!strcmp(dp->d_name, "boot"))
			continue;

		if (!strcmp(dp->d_name, ".") || !strcmp(dp->d_name, ".."))
			continue;

		run_or_ret("cp -a %s/%s %s/", MOUNTPOINT_INSTALL_DATA,
			   dp->d_name, MOUNTPOINT_ROOT);
	}

	closedir(dfd);
	return 0;
}

int od2_install_step_copy_files_root_progress(void)
{
	struct statvfs i = {0}; /* install data (source) */
	struct statvfs r = {0}; /* root (target) */
	uint64_t i_used_bytes;
	uint64_t r_used_bytes;

	if (statvfs(MOUNTPOINT_INSTALL_DATA, &i) != 0) {
		perror("WARNING: statvfs failed on " MOUNTPOINT_INSTALL_DATA);
		return 0;
	}

	if (statvfs(MOUNTPOINT_ROOT, &r) != 0) {
		perror("WARNING: statvfs failed on " MOUNTPOINT_ROOT);
		return 0;
	}

	i_used_bytes = (i.f_blocks - i.f_bfree) * i.f_frsize;
	r_used_bytes = (r.f_blocks - r.f_bfree) * r.f_frsize;

	return 100 * r_used_bytes / i_used_bytes;
}

int od2_install_step_mount_boot(void)
{
	const char *mount_boot_inst = get_boot_mountpoint_installer();
	enum od2_storage_id target_id = od2_ui_get_target_storage();

	run_or_ret("mkdir %s", MOUNTPOINT_BOOT);

	if (od2_storage_is_boot_dev(target_id)) {
		run_or_ret("mount -o remount,rw %s", mount_boot_inst);
		run_or_ret("mount --bind %s %s", mount_boot_inst,
			   MOUNTPOINT_BOOT);
	} else {
		const char *boot_part = get_boot_partition();
		if (!boot_part)
			return -1;

		run_or_ret("mount %s %s", boot_part, MOUNTPOINT_BOOT);
	}
	return 0;
}

/* Copy files from /boot of the installer to the new boot partition of the
 * installed system.
 * If it makes more sense for your OS to have different files in /boot, we
 * could add an option to ondev2.cfg so it will copy the files from the /boot
 * dir of the installation data instead. It would be trivial to implement, but
 * not sure if any OS will need it so leaving it out to reduce complexity. */
int od2_install_step_copy_files_boot(void)
{
	enum od2_storage_id target_id = od2_ui_get_target_storage();

	/* Installing to the storage we booted from, the boot partition already
	 * has the same files as /boot of the installer. */
	if (od2_storage_is_boot_dev(target_id))
		return 0;

	/* Copy files from /boot of the installer */
	run_or_ret("cp -a %s/* %s/", get_boot_mountpoint_installer(),
		   MOUNTPOINT_BOOT);

	return 0;
}

/* TODO: support selecting different file systems */
int od2_install_step_update_fstab(void)
{
	struct mntent mntent = {0};
	FILE *f;

	/* If fstab doesn't exist, create it with a comment. Otherwise delete
	 * the existing root and boot partition entries. */
	if (access(PATH_ETC_FSTAB, F_OK) != 0)
		run_or_ret("echo '# <file system> <mount point> <type>"
			   " <options> <dump> <pass>' > '%s'",
			   PATH_ETC_FSTAB);
	else
		run_or_ret("ondev2-fstab-remove-boot-root %s", PATH_ETC_FSTAB);

	/* Open fstab to write new entries */
	f = setmntent(PATH_ETC_FSTAB, "a");
	if (!f) {
		perror("ERROR: setmntent failed on " PATH_ETC_FSTAB);
		return -1;
	}

	/* Write root entry */
	if (od2_ui_is_fde_selected())
		mntent.mnt_fsname = "/dev/mapper/root";
	else
		mntent.mnt_fsname = (char *)od2_storage_get_uuid_str(get_root_partition(false));
	mntent.mnt_dir = "/";
	mntent.mnt_type = "ext4";
	mntent.mnt_opts = "rw";
	if (addmntent(f, &mntent)) {
		printf("ERROR: addmntent failed for root\n");
		goto error;
	}

	/* Write boot entry */
	mntent.mnt_fsname = (char *)od2_storage_get_uuid_str(get_boot_partition());
	mntent.mnt_dir = "/boot";
	mntent.mnt_type = "ext4";
	mntent.mnt_opts = "rw";
	if (addmntent(f, &mntent)) {
		printf("ERROR: addmntent failed for boot\n");
		goto error;
	}

	endmntent(f);

	/* Print the finished file for the log */
	run_or_ret("cat %s", PATH_ETC_FSTAB);

	return 0;
error:
	endmntent(f);
	return -1;
}

int od2_install_step_update_crypttab(void)
{
	run_or_ret("echo 'root %s none luks' >> %s",
		   od2_storage_get_uuid_str(get_root_partition(false)),
		   PATH_ETC_CRYPTTAB);

	return 0;
}

int od2_install_step_prepare_chroot(void)
{
	/* Prepare /dev */
	run_or_ret("mkdir -p %s/dev", MOUNTPOINT_ROOT);
	run_or_ret("mount -t tmpfs -o size=1M,noexec,dev tmpfs %s/dev",
		   MOUNTPOINT_ROOT);
	run_or_ret("mkdir -p %s/dev/pts %s/dev/shm", MOUNTPOINT_ROOT,
		   MOUNTPOINT_ROOT);
	run_or_ret("mount -t tmpfs -o nodev,nosuid,noexec tmpfs %s/dev/shm",
		   MOUNTPOINT_ROOT);

	/* Create nodes in /dev */
	run_or_ret("mknod -m 666 %s/dev/null c 1 3", MOUNTPOINT_ROOT);
	run_or_ret("mknod -m 666 %s/dev/zero c 1 5", MOUNTPOINT_ROOT);
	run_or_ret("mknod -m 666 %s/dev/full c 1 7", MOUNTPOINT_ROOT);
	run_or_ret("mknod -m 644 %s/dev/random c 1 8", MOUNTPOINT_ROOT);
	run_or_ret("mknod -m 644 %s/dev/urandom c 1 9", MOUNTPOINT_ROOT);

	/* Mount /proc */
	run_or_ret("mkdir -p %s/proc", MOUNTPOINT_ROOT);
	run_or_ret("mount --bind /proc %s/proc", MOUNTPOINT_ROOT);

	return 0;
}

int od2_install_step_run_post_install_script(void)
{
	run_or_ret(""
		   " OD2_CHROOT=%s"
		   " OD2_FDE=%i"
		   " %s",
		   MOUNTPOINT_ROOT,
		   od2_ui_is_fde_selected(),
		   g_config.cmd_post_install);

	return 0;
}

int od2_install_step_clean_chroot(void)
{
	run_or_ret("umount %s/dev/shm", MOUNTPOINT_ROOT);
	run_or_ret("umount %s/dev", MOUNTPOINT_ROOT);
	run_or_ret("umount %s/proc", MOUNTPOINT_ROOT);

	return 0;
}

int od2_install_step_umount_boot_root_install_data(void)
{
	run_or_ret("umount %s", MOUNTPOINT_BOOT);
	run_or_ret("umount %s", MOUNTPOINT_ROOT);
	run_or_ret("umount %s", MOUNTPOINT_INSTALL_DATA);

	return 0;
}

int od2_install_step_umount_root_partition_luks(void)
{
	struct crypt_device *cd;
	int rc;

	rc = crypt_init_by_name(&cd, CRYPTSETUP_ROOT_DEVICE_NAME);
	if (rc < 0) {
		printf("ERROR: crypt_init_by_name() failed: %i\n", rc);
		return rc; /* intentionally no crypt_free() */
	}

	rc = crypt_deactivate(cd, CRYPTSETUP_ROOT_DEVICE_NAME);
	if (rc < 0) {
		printf("ERROR: crypt_deactivate() failed: %i\n", rc);
		goto done;
	}

done:
	crypt_free(cd);
	return rc;
}

int od2_install_step_extend_root_over_install_data(void)
{
	/* FIXME:
	 * parted: delete install data partition (p3)
	 * parted: resize p2 to 100%
	 * if cryptsetup: extend cryptsetup partition, mount cryptsetup
	 * extend root filesystem
	 * umount all again */
	printf("STUB: %s\n", __func__);
	return 0;
}
